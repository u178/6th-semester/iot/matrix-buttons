#include "mbed.h"
#include <cstdio>
#include <string>

#define MAXIMUM_BUFFER_SIZE 32

BusInOut rows(PA_1, PA_2, PA_3, PA_4);
BusInOut cols(PB_3, PB_4, PB_5);

static BufferedSerial serial_port(PA_9, PA_10);
FileHandle *mbed::mbed_override_console(int fd) {
    return &serial_port;
}


int get_row(int true_bits) {
    switch (true_bits) {
        case 0x4: return 1;
        case 0x2: return 2;
        case 0x1: return 3;
        case 0x8: return 4;
    }
    return 0;
}

int get_collumn(int true_bits) {
    switch (true_bits) {
        case 0x3: return 1;
        case 0x5: return 2;
        case 0x6: return 3; 
    }
    return 0;
}

int main() {
    serial_port.set_baud(9600);
    serial_port.set_format(8, BufferedSerial::None, 1);
    
    //rows.mode(PullUp);
    rows.mode(PullDown);
    cols.mode(PullUp);


    int prev_row = 0;
    int prev_col = 0;

    while (1) {


        rows.input();
        cols.output();
        int row = get_row(rows & rows.mask());
        //printf("%d%c", rows & rows.mask(), '\n');

        rows.output();
        cols.input();
        int col = get_collumn(cols & cols.mask());
        //printf("%d%c", cols & cols.mask(), '\n');

        if ((prev_row != row || prev_col != col) && row != 0 && col != 0) {
            prev_col = 0;
            prev_row = 0;
            //printf("%d%c", rows, '\n')
            printf("%s%d%s%d%c", "Row: ", row, ", col: ", col, '\n');
        }
        ThisThread::sleep_for(1000);
    }
}